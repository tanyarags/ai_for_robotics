# ----------
# User Instructions:
# 
# Implement the function optimum_policy2D below.
#
# You are given a car in grid with initial state
# init. Your task is to compute and return the car's 
# optimal path to the position specified in goal; 
# the costs for each motion are as defined in cost.
#
# There are four motion directions: up, left, down, and right.
# Increasing the index in this array corresponds to making a
# a left turn, and decreasing the index corresponds to making a 
# right turn.

forward = [[-1,  0], # go up

           [ 0, -1], # go left
           [ 1,  0], # go down
           [ 0,  1]] # go right
forward_name = ['up', 'left', 'down', 'right']

# action has 3 values: right turn, no turn, left turn
action = [-1, 0, 1]
action_name = ['R', '#', 'L']

# EXAMPLE INPUTS:
# grid format:
#     0 = navigable space
#     1 = unnavigable space 
grid = [[1, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 1, 0],
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1],
        [1, 1, 1, 0, 1, 1]]

init = [4, 3, 0] # given in the form [row,col,direction]
                 # direction = 0: up
                 #             1: left
                 #             2: down
                 #             3: right
                
goal = [2, 0] # given in the form [row,col]

cost = [2, 1, 2] # cost has 3 values, corresponding to making 
                  # a right turn, no turn, and a left turn

# EXAMPLE OUTPUT:
# calling optimum_policy2D with the given parameters should return 
# [[' ', ' ', ' ', 'R', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', '#'],
#  ['*', '#', '#', '#', '#', 'R'],
#  [' ', ' ', ' ', '#', ' ', ' '],
#  [' ', ' ', ' ', '#', ' ', ' ']]
# ----------

# ----------------------------------------
# modify code below
# ----------------------------------------

def optimum_policy2D(grid,init,goal,cost):

    
    value = [[[999 for row in range(len(grid[0]))] for col in range(len(grid))] for depth in range(len(forward))]    
    #global policy
    policy = [[[ ' ' for row in range(len(grid[0]))] for col in range(len(grid))] for depth in range(len(forward))]
    
    # print len(policy)
    # print len(policy[0])
    # print len(policy[0][0])
    # for p in range(len(policy)):
    #     for y in range(len(policy[0])):
    #         print policy[p][y]
    #     print '\n'
    
    #Optimum policy, to be constructed from global policy
    policy2D = [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]

    x = goal[0]
    y = goal[1]
    g = 0

    open = []
    for ori in range(len(forward)): #trying to reach goal from any direction
        open.append([g, x, y, ori])
        policy[ori][x][y] = '*'
    resign = False # flag set if we can't find expand
    
    while not resign:
        if len(open) == 0:
            resign = True
        else:
            open.sort()
            open.reverse()
            next = open.pop()
            x = next[1]
            y = next[2]
            ori = next[3]
            g = next[0]
            value[ori][x][y] = g
            
            for i in range(len(action)): #since considering only forward motion
                ori2 = (ori - action[i])%len(forward) 
                x2 = x - forward[ori][0]
                y2 = y - forward[ori][1]
                
                if x2 >= 0 and x2 < len(grid) and y2 >=0 and y2 < len(grid[0]):
                    if value[ori2][x2][y2] > (value[ori][x][y]+ cost[i]) and grid[x2][y2] == 0:
                        g2 = g + cost[i]
                        open.append([g2, x2, y2, ori2])
                        policy[ori2][x2][y2] = action_name[i]

    # for p in range(len(policy)):
    #     for y in range(len(policy[0])):
    #         print policy[p][y]    
    #construction optimum policy from global policy now.

    nodeReached = False
    x_r = init[0]
    y_r = init[1]
    ori_r = init[2]
    
    while nodeReached == False:
        policy2D[x_r][y_r] = policy[ori_r][x_r][y_r]
        # print x_r, y_r, ori_r
        act = 1    
        
        if policy[ori_r][x_r][y_r] == 'L':
            act = 2
        elif policy[ori_r][x_r][y_r] == 'R':
            act = 0
        elif policy[ori_r][x_r][y_r] == '*':
            nodeReached = True
            
        ori_r = (ori_r + action[act])%len(forward)    
        x_r = x_r + forward[ori_r][0]
        y_r = y_r + forward[ori_r][1]
        
        
    return policy2D

pol = optimum_policy2D(grid,init,goal,cost)

for p in range(len(pol)):
    print pol[p]
