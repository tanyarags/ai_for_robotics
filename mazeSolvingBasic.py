# ----------
# User Instructions:
# 
# Define a function, search() that returns a list
# in the form of [optimal path length, row, col]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]
init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1]
cost = 1

delta = [[-1, 0], # go up
         [ 0,-1], # go left
         [ 1, 0], # go down
         [ 0, 1]] # go right

delta_name = ['^', '<', 'v', '>']

def search(grid,init,goal,cost):
    # ----------------------------------------
    # insert code here
    # ----------------------------------------
    path = []
    
    openlist = []
    openlist.append([0, init[0], init[1]])
    grid_visited = grid #adding 2 for visited nodes
    
    while openlist != [] :
        openlist = sorted(openlist, key=lambda openlist: openlist[0])
        
        curr = openlist[0]
        #print 'current ', curr
        del openlist[0]
        #print openlist
        
        grid_point = [curr[1], curr[2]]
        
        if goal == grid_point:
            path = curr
            openlist = []
        else : 
            visited  = grid_visited[curr[1]][curr[2]]
            
            if visited != 2:
                #add successors with updated gvalue to open list
                grid_visited[curr[1]][curr[2]] = 2
                
                for direction in range(len(delta)):
                    
                    neighbour = [ grid_point[0] + delta[direction][0], grid_point[1] + delta[direction][1]]
                    #print 'Direction', delta[direction]
                    if (0 <= neighbour[0] < len(grid)) and ( 0 <= neighbour[1] < len(grid[0])) and (grid_visited[neighbour[0]][neighbour[1]] == 0): 
                        #print 'neighbour', neighbour
                        openlist.append([curr[0]+1, neighbour[0], neighbour[1]])
                
                
    if path == []: 
        print 'fail'
        return 'fail'
    else : 
        print path
        
    return path
    
    
search(grid,init,goal,cost)
