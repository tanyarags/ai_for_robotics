# --------------
# USER INSTRUCTIONS
#
# Write a function called stochastic_value that 
# returns two grids. The first grid, value, should 
# contain the computed value of each cell as shown 
# in the video. The second grid, policy, should 
# contain the optimum policy for each cell.
#
# --------------
# GRADING NOTES
#
# We will be calling your stochastic_value function
# with several different grids and different values
# of success_prob, collision_cost, and cost_step.
# In order to be marked correct, your function must
# RETURN (it does not have to print) two grids,
# value and policy.
#
# When grading your value grid, we will compare the
# value of each cell with the true value according
# to this model. If your answer for each cell
# is sufficiently close to the correct answer
# (within 0.001), you will be marked as correct.

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>'] # Use these when creating your policy grid.

# ---------------------------------------------
#  Modify the function stochastic_value below
# ---------------------------------------------

def stochastic_value(grid,goal,cost_step,collision_cost,success_prob):
    failure_prob = (1.0 - success_prob)/2.0 # Probability(stepping left) = prob(stepping right) = failure_prob
    value = [[collision_cost for col in range(len(grid[0]))] for row in range(len(grid))]
    policy = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
    value_back = [[collision_cost for col in range(len(grid[0]))] for row in range(len(grid))]
        
    
    convergence = False
    while convergence == False :
        
        for i in range(len(value)):
            for j in range(len(value[0])):
                value_back[i][j] = value[i][j]
        
        x = goal[0]
        y = goal[1]
        g = 0

        value[goal[0]][goal[1]] = 0
        open = []
        open.append([g, x, y])
        policy[x][y] = '*'
        resign = False # flag set if we can't find expand
    
        while not resign:
            if len(open) == 0:
                resign = True
            else:
                open.sort()
                open.reverse()
                next = open.pop()
                x = next[1]
                y = next[2]
                g = next[0]
                value[x][y] = g
            
                #print x, y, g
            
                for i in range(len(delta)):
                    x2 = x - delta[i][0]
                    y2 = y - delta[i][1]
                
                    if x2 >= 0 and x2 < len(grid) and y2 >=0 and y2 < len(grid[0]):
                        cost_stochastic = cost_step
                    
                        #success case
                        cost_stochastic = cost_stochastic + (value[x][y] * success_prob)
                    
                        #failure cases
                        for surr in [-1, 1]:
                            #adding the surrounding's cost
                            neighX = x2 + delta[(i + surr)%len(delta)][0]
                            neighY = y2 + delta[(i + surr)%len(delta)][1]
                        
                            if neighX >= 0 and neighX < len(grid) and neighY >=0 and neighY < len(grid[0]):
                                neigh_cost = value[neighX][neighY] * failure_prob
                     
                            else: 
                                neigh_cost = collision_cost * failure_prob
                     
                            cost_stochastic = cost_stochastic + neigh_cost 
                                            
                        #print x2, y2, (value[x][y]+ cost_stochastic)            
                        if value[x2][y2] > cost_stochastic and grid[x2][y2] == 0:
                            g2 = cost_stochastic
                            open.append([g2, x2, y2])
                            policy[x2][y2] = delta_name[i]
        
        sum_err = 0
        for i in range(len(value)):
            for j in range(len(value[0])):
                sum_err = sum_err + abs(value_back[i][j] - value[i][j])
        if sum_err < 0.001:
            convergence = True 
        for row in value:
            print row
        for row in policy:
            print row    
    return value, policy

# ---------------------------------------------
#  Use the code below to test your solution
# ---------------------------------------------

grid = [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0]]
goal = [0, len(grid[0])-1] # Goal is in top right corner
cost_step = 1
collision_cost = 100
success_prob = 0.5

value,policy = stochastic_value(grid,goal,cost_step,collision_cost,success_prob)
for row in value:
    print row
for row in policy:
    print row

# Expected outputs:
#
# [57.9029, 40.2784, 26.0665,  0.0000]
# [47.0547, 36.5722, 29.9937, 27.2698]
# [53.1715, 42.0228, 37.7755, 45.0916]
# [77.5858, 100.00, 100.00, 73.5458]
#
# ['>', 'v', 'v', '*']
# ['>', '>', '^', '<']
# ['>', '^', '^', '<']
# ['^', ' ', ' ', '^']

